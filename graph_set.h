#ifndef GRAPH_SET_H
#define GRAPH_SET_H
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include <math.h>
#include <time.h>
#include "table.h"
#include "graph_set_q.h"
#include "graph_set_todos.h"
#define EDGE_SET_ITEM_LEN 128
#define EDGE_SET_TABLE_CAPACITY 2048
#define EDGE_SET_COLS_UNIT 8
#define EDGE_SET_COLS_OFFSET 7
#define EDGE_SET_GAIN 0
#define EDGE_SET_FAIL 1
#define EDGE_SET_CHANCE_MAX 100
// TODO may not need this depends on nodes number
typedef unsigned int EDGE_SET_UINT32;
typedef unsigned char EDGE_SET_UCHAR;
typedef struct vertex{
  EDGE_SET_UINT32 hash_code;
  // a code to identify this entity
  // assume hash code produced at index 0 item
  char type[EDGE_SET_ITEM_LEN];
  // type
  char title[EDGE_SET_ITEM_LEN][EDGE_SET_ITEM_LEN];
  // item titles
  char item[EDGE_SET_ITEM_LEN][EDGE_SET_ITEM_LEN];
  // items ~ can be alphanumerical
}Vertex;
typedef struct edge_set{
  EDGE_SET_UCHAR **table; 
  // table ~ it is square in terms of bits required
  EDGE_SET_UINT32 ros; 
  // ros ~ number of ros
  EDGE_SET_UINT32 cols;
  // cols ~ number of cols
  char description[EDGE_SET_ITEM_LEN]; 
  // description ~ relation name
}EdgeSet;
// vertex set ~ it is just a hash table
Vertex* vertex_make();
void vertex_set(Vertex *this,EDGE_SET_UINT32 hash_code,char type[EDGE_SET_ITEM_LEN],char title[EDGE_SET_ITEM_LEN],char item[EDGE_SET_ITEM_LEN],int index);
void vertex_copy(Vertex *this, Vertex *one);
void vertex_destroy(Vertex **this);
void vertex_set_union(Table *this,Table *one, Table *other);
void vertex_set_intersect(Table *this,Table *one, Table *other);
void vertex_set_destroy(Table *this);
EdgeSet* edge_set_make(EDGE_SET_UINT32 ros, EDGE_SET_UINT32 cols,char *description);
void edge_set_on(EdgeSet* this, EDGE_SET_UINT32 ro, EDGE_SET_UINT32 col);
void edge_set_off(EdgeSet* this, EDGE_SET_UINT32 ro, EDGE_SET_UINT32 col);
void edge_set_union(EdgeSet* this, EdgeSet* one, EdgeSet* other);
void edge_set_intersect(EdgeSet* this, EdgeSet* one, EdgeSet* other);
void edge_set_complement(EdgeSet* this, EdgeSet* one);
void edge_set_bellman(EdgeSet *this, int from, EDGE_SET_UINT32 *path, EDGE_SET_UINT32 *distance);
void edge_set_bfs(EdgeSet *this,int from, EDGE_SET_UINT32 *path, EDGE_SET_UINT32 *distance,Q* q, Qdo* qdo);
void edge_set_dfs(EdgeSet *this,int from,int count,EDGE_SET_UINT32 *path,EDGE_SET_UINT32 *distance);
int edge_set_is_connected(int ros,EDGE_SET_UINT32 *distance);
EDGE_SET_UINT32 edge_set_max_distance(int ros,EDGE_SET_UINT32 *distance);
void edge_set_tree(int ros,EDGE_SET_UINT32*distance,EDGE_SET_UINT32 max_distance);
void edge_set_path(EdgeSet*this);
void edge_set_random(EdgeSet*this,float chance);
void edge_set_destroy(EdgeSet **this);
#endif
