#
# Makefile 
# graph sets
#

CC=gcc
AR=ar
CFLAGS=-g -std=c99 -Werror -Wall -Wextra -Wformat -Wformat-security -pedantic -DMORE
OBJ_FLAGS=-c -static -g
AR_FLAGS=rcs
LIBS=-ltable -lm -lgraphsetq

FILES=graph_set.h graph_set.c graph_set_vertex_make_test.c
FILES_ESET_MAKE=graph_set.h graph_set.c graph_set_eset_make_test.c
FILES_ESET_ON=graph_set.h graph_set.c graph_set_eset_on_test.c
FILES_ESET_UNION=graph_set.h graph_set.c graph_set_eset_union_test.c
FILES_ESET_INTERSECT=graph_set.h graph_set.c graph_set_eset_intersect_test.c
FILES_ESET_COMPLEMENT=graph_set.h graph_set.c graph_set_eset_complement_test.c
FILES_ESET_BELLMAN=graph_set.h graph_set.c graph_set_eset_bellman_test.c
FILES_ESET_BFS=graph_set.h graph_set.c graph_set_eset_bfs_test.c
FILES_ESET_PATH=graph_set.h graph_set.c graph_set_eset_path_test.c
FILES_ESET_RANDOM=graph_set.h graph_set.c graph_set_eset_random_test.c
FILES_VSET_UNION=graph_set.h graph_set.c graph_set_vset_test.c
FILES_VSET_INTERSECT=graph_set.h graph_set.c graph_set_vset_intersect_test.c
FILES_ESET_EIGHTK=graph_set.h graph_set.c graph_set_eset_eightk_test.c
FILES_ESET_EIGHTKSPARSE=graph_set.h graph_set.c graph_set_eset_eightksparse_test.c
FILES_ESET_DFS=graph_set.h graph_set.c graph_set_eset_dfs_test.c
CLEAN=graph_set_vertex_make_test graph_set_eset_make_test graph_set_eset_on_test graph_set_eset_union_test graph_set_eset_intersect_test graph_set_eset_complement_test graph_set_eset_bellman_test graph_set_eset_bfs_test graph_set_eset_path_test graph_set_eset_random_test graph_set_vset_test graph_set_vset_intersect_test  graph_set_eset_dfs_test

CLEAN_ESET_EIGHTK=graph_set_eset_eightk_test
CLEAN_ESET_EIGHTKSPARSE=graph_set_eset_eightksparse_test

all: main obj lib tst_vert_make main_eset_make tst_eset_make main_eset_on tst_eset_on main_eset_union tst_eset_union main_eset_intersect tst_eset_intersect main_eset_complement tst_eset_complement main_eset_bellman tst_eset_bellman main_eset_bfs tst_eset_bfs main_eset_path tst_eset_path main_eset_random tst_eset_random main_vset_union tst_vset_union main_vset_intersect tst_vset_intersect main_eset_dfs tst_eset_dfs

eightk: main_eset_eightk tst_eset_eightk
eightk_sparse: main_eset_eightksparse tst_eset_eightksparse

main: ${FILES}
	${CC} ${CFLAGS} graph_set.c graph_set_vertex_make_test.c ${LIBS} -o graph_set_vertex_make_test

obj: ${FILES}
	${CC} ${OBJ_FLAGS} graph_set.c -o graph_set.o

lib: ${FILES}
	${AR} ${AR_FLAGS} libgraphset.a graph_set.o

tst_vert_make: ${FILES}
	./graph_set_vertex_make_test < graph_set_vertex_make_test.txt

main_eset_make: ${FILES_ESET_MAKE}
	${CC} ${CFLAGS} graph_set.c graph_set_eset_make_test.c ${LIBS} -o graph_set_eset_make_test

tst_eset_make: ${FILES_ESET_MAKE}
	./graph_set_eset_make_test < graph_set_eset_make_test.txt

main_eset_on: ${FILES_ESET_ON}
	${CC} ${CFLAGS} graph_set.c graph_set_eset_on_test.c ${LIBS} -o graph_set_eset_on_test

tst_eset_on: ${FILES_ESET_ON}
	./graph_set_eset_on_test < graph_set_eset_on_test.txt

main_eset_union: ${FILES_ESET_UNION}
	${CC} ${CFLAGS} graph_set.c graph_set_eset_union_test.c ${LIBS} -o graph_set_eset_union_test

tst_eset_union: ${FILES_ESET_UNION}
	./graph_set_eset_union_test < graph_set_eset_union_test.txt

main_eset_intersect: ${FILES_ESET_INTERSECT}
	${CC} ${CFLAGS} graph_set.c graph_set_eset_intersect_test.c ${LIBS} -o graph_set_eset_intersect_test

tst_eset_intersect: ${FILES_ESET_INTERSECT}
	./graph_set_eset_intersect_test < graph_set_eset_intersect_test.txt

main_eset_complement: ${FILES_ESET_COMPLEMENT}
	${CC} ${CFLAGS} graph_set.c graph_set_eset_complement_test.c ${LIBS} -o graph_set_eset_complement_test

tst_eset_complement: ${FILES_ESET_COMPLEMENT}
	./graph_set_eset_complement_test < graph_set_eset_complement_test.txt

main_eset_bellman: ${FILES_ESET_BELLMAN}
	${CC} ${CFLAGS} graph_set.c graph_set_eset_bellman_test.c ${LIBS} -o graph_set_eset_bellman_test

tst_eset_bellman: ${FILES_ESET_BELLMAN}
	./graph_set_eset_bellman_test < graph_set_eset_bellman_test.txt

main_eset_bfs: ${FILES_ESET_BFS}
	${CC} ${CFLAGS} graph_set.c graph_set_eset_bfs_test.c ${LIBS} -o graph_set_eset_bfs_test

tst_eset_bfs: ${FILES_ESET_BFS}
	./graph_set_eset_bfs_test < graph_set_eset_bfs_test.txt

main_eset_path: ${FILES_ESET_PATH}
	${CC} ${CFLAGS} graph_set.c graph_set_eset_path_test.c ${LIBS} -o graph_set_eset_path_test

tst_eset_path: ${FILES_ESET_PATH}
	./graph_set_eset_path_test < graph_set_eset_path_test.txt

main_eset_random: ${FILES_ESET_RANDOM}
	${CC} ${CFLAGS} graph_set.c graph_set_eset_random_test.c ${LIBS} -o graph_set_eset_random_test

tst_eset_random: ${FILES_ESET_RANDOM}
	./graph_set_eset_random_test < graph_set_eset_random_test.txt

main_vset_union: ${FILES_VSET_UNION}
	${CC} ${CFLAGS} graph_set.c graph_set_vset_test.c ${LIBS} -o graph_set_vset_test

tst_vset_union: ${FILES_VSET_UNION}
	./graph_set_vset_test < graph_set_vset_test.txt

main_vset_intersect: ${FILES_VSET_INTERSECT}
	${CC} ${CFLAGS} graph_set.c graph_set_vset_intersect_test.c ${LIBS} -o graph_set_vset_intersect_test

tst_vset_intersect: ${FILES_VSET_INTERSECT}
	./graph_set_vset_intersect_test < graph_set_vset_intersect_test.txt

main_eset_eightk: ${FILES_ESET_EIGHTK}
	${CC} ${CFLAGS} graph_set.c graph_set_eset_eightk_test.c ${LIBS} -o graph_set_eset_eightk_test

tst_eset_eightk: ${FILES_ESET_EIGHTK}
	./graph_set_eset_eightk_test < graph_set_eset_eightk_test.txt

main_eset_eightksparse: ${FILES_ESET_EIGHTKSPARSE}
	${CC} ${CFLAGS} graph_set.c graph_set_eset_eightksparse_test.c ${LIBS} -o graph_set_eset_eightksparse_test

tst_eset_eightksparse: ${FILES_ESET_EIGHTKSPARSE}
	./graph_set_eset_eightksparse_test < graph_set_eset_eightksparse_test.txt

main_eset_dfs: ${FILES_ESET_DFS}
	${CC} ${CFLAGS} graph_set.c graph_set_eset_dfs_test.c ${LIBS} -o graph_set_eset_dfs_test

tst_eset_dfs: ${FILES_ESET_DFS}
	./graph_set_eset_dfs_test < graph_set_eset_dfs_test.txt

clean: ${CLEAN}
	rm $^

clean_eightk: ${CLEAN_ESET_EIGHTK}
	rm $^

clean_eightksparse: ${CLEAN_ESET_EIGHTKSPARSE}
	rm $^
