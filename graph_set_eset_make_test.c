#include "graph_set.h"
int main(){
  int ros;
  int cols;
  char description[EDGE_SET_ITEM_LEN];
  EdgeSet *edge_set;
  fscanf(stdin,"%d",&ros);
  fscanf(stdin,"%d",&cols);
  fscanf(stdin,"%s",description);
  edge_set = edge_set_make(ros,ceil(cols/EDGE_SET_COLS_UNIT),description);
  assert((int)(edge_set->ros) == (int)ros);
  assert((int)(edge_set->cols) == (int)(ceil(cols/EDGE_SET_COLS_UNIT)));
  assert(strcmp(edge_set->description, description) == 0);
  printf("%d %d %s\n",edge_set->ros, edge_set->cols, edge_set->description);
  edge_set_destroy(&edge_set);
  return 0;
}
