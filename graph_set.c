#include "graph_set.h"
/*
* Function: vertex_make
* 
* make vertex instance
*
* return: pointer to a vertex
*/
Vertex* vertex_make() {
  Vertex *this;
  this=(Vertex*)calloc(1,sizeof(Vertex));
  return this;
}
/*
* Function: vertex_set
* 
* change vertex properties
*
* return: nothing
*/
void vertex_set(Vertex *this,EDGE_SET_UINT32 hash_code,char type[EDGE_SET_ITEM_LEN],char title[EDGE_SET_ITEM_LEN],char item[EDGE_SET_ITEM_LEN],int index){
  this->hash_code=hash_code;
  memcpy(this->type,type,strlen(type));
  memcpy(this->title[index],title,strlen(title));
  memcpy(this->item[index],item,strlen(item));
}
/*
* Function: vertex_copy
* 
* copy a vertex
*
* return: void
*/
void vertex_copy(Vertex *this, Vertex *one){
  int i;
  this->hash_code=one->hash_code;
  memcpy(this->type,one->type,strlen(this->type));
  for(i=0;i<EDGE_SET_ITEM_LEN;i++){
    memcpy(this->title[i],one->title[i],strlen(this->title[i]));
    memcpy(this->item[i],one->item[i],strlen(this->item[i]));
  }
}
/*
* Function: vertex_destroy
* 
* destroys a vertex
*
* return: void
*/
void vertex_destroy(Vertex **this){
  free(*this);
  *this = NULL;
}
/*
* Function: vertex_set_union
* 
* an union of hash tables
*
* return: void
*/
void vertex_set_union(Table *this,Table *one, Table *other){
  int i,capacity;
  capacity=one->capacity;
  for(i=0;i<capacity;i++){
    if(one->value[i]!=NULL){
      this->value[i]=vertex_make();
      vertex_copy(this->value[i],one->value[i]);
      continue;
    }
    if(other->value[i]!=NULL){
      this->value[i]=vertex_make();
      vertex_copy(this->value[i],other->value[i]);
    }
  }
}
/*
* Function: vertex_set_intersect
* 
* an intersect of hash tables
*
* return: void
*/
void vertex_set_intersect(Table *this,Table *one, Table *other){
  int i,capacity;
  capacity=one->capacity;
  for(i=0;i<capacity;i++){
    if((one->value[i]==NULL) || (other->value[i]==NULL)){
      continue;
    }
    this->value[i]=vertex_make();
    vertex_copy(this->value[i],one->value[i]);
  }
}
/*
* Function: vertex_set_destroy
* 
* destroy vertex set (a hash table)
*
* return: nothing
*/
void vertex_set_destroy(Table *this){
  int i,capacity;
  Vertex *t;
  capacity=this->capacity;
  for(i=0;i<capacity;i++){
    if(this->value[i]==NULL){
      continue;
    }
    t=(Vertex*)this->value[i];
    vertex_destroy(&t);
  }
}
/*
* Function: edge_set_make
* 
* makes an edge set
*
* return: a pointer to an edge set
*/
EdgeSet* edge_set_make(EDGE_SET_UINT32 ros, EDGE_SET_UINT32 cols, char *description) {
  EdgeSet* this = (EdgeSet*)calloc(1,sizeof(EdgeSet));
  EDGE_SET_UINT32 i;
  this->ros=ros;
  this->cols=cols;
  memcpy(this->description,description,strlen(description));
  this->table=(EDGE_SET_UCHAR**)calloc(this->ros,sizeof(EDGE_SET_UCHAR*));
  for(i=0;i<this->ros;i++){
    this->table[i]=(EDGE_SET_UCHAR*)calloc(this->cols,sizeof(EDGE_SET_UCHAR));
  }
  return this;
}
/*
* Function: edge_set_on
* 
* create an edge
*
* return: nothing
*/
void edge_set_on(EdgeSet* this, EDGE_SET_UINT32 ro, EDGE_SET_UINT32 col) {
  EDGE_SET_UINT32 index = col/EDGE_SET_COLS_UNIT;
  EDGE_SET_UINT32 remainder = col%EDGE_SET_COLS_UNIT;
  EDGE_SET_UCHAR mask = 0x01;
  mask = mask<<(EDGE_SET_COLS_OFFSET-remainder);
  this->table[ro][index] = this->table[ro][index] | mask;
}
/*
* Function: edge_set_off
* 
* remove an edge
*
* return: nothing
*/
void edge_set_off(EdgeSet* this, EDGE_SET_UINT32 ro, EDGE_SET_UINT32 col) {
  EDGE_SET_UINT32 index = col/EDGE_SET_COLS_UNIT;
  EDGE_SET_UINT32 remainder = col%EDGE_SET_COLS_UNIT;
  EDGE_SET_UCHAR mask = 0x01;
  mask = mask<<(EDGE_SET_COLS_OFFSET-remainder);
  this->table[ro][index] = this->table[ro][index] ^ mask;
}
/*
* Function: edge_set_union
* 
* create an union
*
* return: nothing
*/
void edge_set_union(EdgeSet* this, EdgeSet* one, EdgeSet* other){
  int ros,cols,i,j;
  ros = one->ros;
  cols = one->cols;
  for(i=0;i<ros;i++) {
    for(j=0;j<cols;j++) {
      this->table[i][j] = one->table[i][j] | other->table[i][j];
    }
  }
}
/*
* Function: edge_set_intersect
* 
* create an intersect
*
* return: nothing
*/
void edge_set_intersect(EdgeSet* this, EdgeSet* one, EdgeSet* other){
  int ros,cols,i,j;
  ros = one->ros;
  cols = one->cols;
  for(i=0;i<ros;i++) {
    for(j=0;j<cols;j++) {
      this->table[i][j] = one->table[i][j] & other->table[i][j];
    }
  }
}
/*
* Function: edge_set_complement
* 
* create a complement
*
* return: nothing
*/
void edge_set_complement(EdgeSet* this, EdgeSet* one){
  int ros,cols,i,j;
  EDGE_SET_UCHAR mask;
  ros = one->ros;
  cols = one->cols;
  mask=0xff;
  for(i=0;i<ros;i++) {
    for(j=0;j<cols;j++) {
      this->table[i][j] = one->table[i][j] ^ mask;
    }
  }
}
/*
* Function: edge_set_bellman
* 
* Bellman-Ford algorithm
*
* shortest/longest path from single source
*
* return: nothing
*/
void edge_set_bellman(EdgeSet *this, int from, EDGE_SET_UINT32 *path, EDGE_SET_UINT32 *distance) {
  int ros,cols,i,updates,ro,col,position;
  EDGE_SET_UINT32 dist;
  EDGE_SET_UCHAR current,mask,remainder;
  ros=this->ros;
  cols=this->cols;
  mask=0x01;
  for(i=0;i<ros;i++){
    distance[i]=INT_MAX;
  }
  distance[from]=0;
  updates=1;
  for(;updates==1;){
    updates=0;
    for(ro=0;ro<ros;ro++){
      for(col=0;col<cols;col++) {
        if(this->table[ro][col]==0) {
          continue;
        }
        remainder=EDGE_SET_COLS_OFFSET;
        current=this->table[ro][col];
        for(;current>0;){
          if((current&mask) > 0) {
            position=EDGE_SET_COLS_UNIT*col+remainder;
            dist=distance[ro]+1;
            if(dist < distance[position]) {
              distance[position]=dist;
              path[position]=ro;
              updates=1;
            }
          }
          current=current>>1;
          remainder--;
        }
      }
    }
  }
}
/*
* Function: edge_set_bfs
* 
* Breadth first search on an edge set
*
* can be used to compute spanning tree (check if graph is connected)
*
* or compute distance from a source
*
* return: nothing
*/
void edge_set_bfs(EdgeSet *this,int from, EDGE_SET_UINT32 *path, EDGE_SET_UINT32 *distance,Q* q, Qdo* qdo){
  int i,ros,cols,ro,position,dist;EDGE_SET_UCHAR current,remainder,mask;
  ros=this->ros;
  cols=this->cols;
  mask=0x01;
  for(i=0;i<ros;i++){
    distance[i]=INT_MAX;
  }
  distance[from]=0;
  qdo->q_push(q,from);
  for(;q->count>0;){
    ro=qdo->q_pop(q);
    for(i=0;i<cols;i++){
      if(this->table[ro][i]==0){
        continue;
      }
      current=this->table[ro][i];
      remainder=EDGE_SET_COLS_OFFSET;
      for(;current>0;){
        if((current&mask)>0){
          position=EDGE_SET_COLS_UNIT*i+remainder;
          dist=distance[ro]+1;
          if(distance[position]==INT_MAX){
            distance[position]=dist;
            path[position]=ro;
            qdo->q_push(q,position);
          }
        }
        current=current>>1;
        remainder--;
      }
    }
  }
}
/*
* Function: edge_set_dfs
* 
* Perform depth first search
*
* can be used to find spanning tree
*
* return: nothing
*/
void edge_set_dfs(EdgeSet *this,int from,int count,EDGE_SET_UINT32 *path,EDGE_SET_UINT32 *distance){
  int i,position,ros;EDGE_SET_UCHAR current,remainder,mask;
  mask=0x01;
  ros=this->ros;
  if(count<=0){
    for(i=0;i<ros;i++){
      distance[i]=INT_MAX;
    }
    distance[from]=0;
  }
  if(count<ros){
    for(i=0;i<ros;i++){
      if(this->table[from][i]==0){
        continue;
      }
      current=this->table[from][i];
      remainder=EDGE_SET_COLS_OFFSET;
      for(;current>0;){
        if((current&mask)>0){
          position=EDGE_SET_COLS_UNIT*i+remainder;
          if(distance[position]==INT_MAX){
            path[position]=from;
            distance[position]=distance[from]+1;
            edge_set_dfs(this,position,count+1,path,distance);
          }
        }
        remainder--;
        current=current>>1;
      }
    }
  }
}
/*
* Function: edge_set_is_connected
* 
* check if graph is connected
*
* return: integer
*/
int edge_set_is_connected(int ros,EDGE_SET_UINT32 *distance){
  int i;
  for(i=0;i<ros;i++){
    if(distance[i]==INT_MAX){
      return EDGE_SET_FAIL;
    }
  }
  return EDGE_SET_GAIN;
}
/*
* Function: edge_set_max_distance
* 
* find maximum distance from the source
*
* return: integer
*/
EDGE_SET_UINT32 edge_set_max_distance(int ros,EDGE_SET_UINT32 *distance){
  EDGE_SET_UINT32 d;int i;
  d=0;
  for(i=0;i<ros;i++){
    if(distance[i]>d){
      d=distance[i];
    }
  }
  return d;
}
/*
* Function: edge_set_tree
* 
* print spanning tree
*
* return: nothing
*/
void edge_set_tree(int ros,EDGE_SET_UINT32*distance,EDGE_SET_UINT32 max_distance){
  EDGE_SET_UINT32 d;int i;
  for(d=0;d<=max_distance;d++){
    if(d>0){
      printf("%*c",d*(EDGE_SET_COLS_UNIT/2),' ');
    }
    for(i=0;i<ros;i++){
      if(distance[i]!=d) {
        continue;
      }
      printf("%d ",i);
    }
    printf("\n");
  }
}
/*
* Function: edge_set_path
* 
* make a connected path
*
* return: nothing
*/
void edge_set_path(EdgeSet*this){
  int i,ros;
  ros=this->ros;
  for(i=0;i<ros-1;i++){
    edge_set_on(this,i,i+1);
  }
}
/*
* Function: edge_set_random
* 
* make a random graph
*
* return: nothing
*/
void edge_set_random(EdgeSet*this,float chance){
  float c;int i,j,ros;
  ros=this->ros;
  for(i=0;i<ros;i++){
    for(j=0;j<ros;j++){
      c=(float)(rand()%EDGE_SET_CHANCE_MAX);
      c=c/EDGE_SET_CHANCE_MAX;
      if(c>=chance) {
        edge_set_on(this,i,j);
      }
    }
  }
}
/*
* Function: edge_set_destroy
* 
* destroys an edge set
*
* return: nothing
*/
void edge_set_destroy(EdgeSet **this){
  EDGE_SET_UINT32 i;
  for(i=0;i<(*this)->ros;i++){
    free((*this)->table[i]);
  }
  free((*this)->table);
  free(*this);
  *this = NULL;
}
